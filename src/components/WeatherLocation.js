import React from 'react';//esto nos permite trabajar con react
import Location from './Location';
import WheatherData from './WeatherData';
import './styles.css';
//ejemplo de una arrow function
var uno = 1;

//arrow function 
const WeatherLocation = () => (

    <div>
        
        <Location city = {"Ciudad Quesda"}></Location>
        <WheatherData></WheatherData>
    </div> 
);


// exportar por defecto, solo una 
export default WeatherLocation ;
