import React from 'react';
import PropTypes from 'prop-types';
const Location = ({city}) => (
    // console.log(props);//imprime en consola la salida de props en este caso 
   //debugger;// hacer un punto de parada en el debugueador del browser hace que se detenga el codigo.
   //cuando se devuelve algo es necesario {} cuando es solo una linea ()
  /*  de esta manera tiene una lidea de codigo un poco mas extensa.  */ 
   // const city =  props.city;// de esta manera se instancia en la constante. 
   /* de la siguiente manera se podra ahorrar un poco menos en el codigo */
   ///const {city} = props;// se libero un poco mas de espacio a esto se le llama destructuring.
  // o aun mas simple y ahorarnos una lidea totalmente se puede poner arriba const Location = (props) lo 
  //siguiente const Location = ({city}) que es el nombre de la variable que estoy parametrizando
  // ({city, OtroParam })
   <div>
        <h1>
            {city}
        </h1>
    </div>
   // si no existe Return el arrowfunction va con () de lo contrario {}
);
Location.protoTypes = {
  city: PropTypes.string.isRequired,
};
///el propType es el que crea un requerimiento del parametro, en este caso un 
//Stirng, pero se puede hacer que el proptype restriccione es number shape que
// que shape es una restriccion de aceptar varios tipos de parametros como Color o  
//el tamanno de la fuente FontSize tambien el que no importa el tipo, pero que es 
//requerido proptype son validaciones 

export default Location;