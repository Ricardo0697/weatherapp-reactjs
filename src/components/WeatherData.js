import  React  from 'react'
import WeatherExtraInfo from './WeatherExtraInfo';
import WeatherTemperature from './WeatherTemperature';

import {
    CLOUD,
    SUN,
    FOG,
    RAIN,
    SNOW,
    CLOUDY,
    WINDY,
} from './../constants/weathers';
import './styles.css';
const WheatherData = () => (
    <div className="weatherdataCont"><WeatherTemperature tempeture = {20} weatherState={CLOUDY}>
    </WeatherTemperature>
    <WeatherExtraInfo humidity={" 80"} wind={" 10 m/s "}>
        </WeatherExtraInfo></div>
);

export default WheatherData;


