export const CLOUD = "cloud";
export const SUN = "sunny";
export const FOG = "fog";
export const RAIN = "rain";
export const SNOW = "snow";
export const CLOUDY = "cloudy";
export const WINDY = "windy";
