import React from 'react';
import PropTypes from 'prop-types';
import './styles.css';
const WeatherExtraInfo  = ({humidity, wind}) => (
    <div className="weatherdataContExtra">
        <span>
             {`Viento ${wind}  -`} 
        </span>
        <span>
            {`${humidity}% `}
        </span>
    </div>
    
);
WeatherExtraInfo.protoTypes = {
   wind: PropTypes.string.isRequired,
   humidity: PropTypes.string.isRequired,
};

export default WeatherExtraInfo;