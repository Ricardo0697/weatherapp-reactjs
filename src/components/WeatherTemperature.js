import React from 'react';
import WeatherIcons from 'react-weathericons';
import PropTypes from 'prop-types';
import './styles.css';
import {
    CLOUD,
    SUN,
    FOG,
    RAIN,
    SNOW,
    CLOUDY,
    WINDY,
} from './../constants/weathers';
//diccionario
const icons ={
  [CLOUD]: "cloud",
  [SUN]: "day-sunny",
  [FOG]: "day-fog",
  [RAIN]: "rain",
  [SNOW]: "snow",
  [CLOUDY]: "cloudy",
  [WINDY]: "windy"
};

const getWeatherIcon = weatherState =>{
    const icon = icons[weatherState];
    if(icon != null ){
        return <WeatherIcons name={icon} size="2x" />

    }else{
    return <WeatherIcons name={"day-sunny"} size="2x" />
}
}
const WeatherTemperature = ({tempeture,weatherState}) => (
    <div className="weatherdataContTempeture"> 
       {
           getWeatherIcon(weatherState)
       }
    <span>{` ${tempeture}Cº`}</span>
    </div>
);
WeatherTemperature.propTypes = {
tempeture: PropTypes.number.isRequired,
weatherState: PropTypes.string.isRequired,
};
export default WeatherTemperature;